
DNS:

Depending on which distro you're using, you might need to tweak your dns resolution.
The goal is that any subdomain of .dev.europa.eu that you try to resolve will be resolved to 127.0.0.1

In Ubuntu 18, this means installing the resolvconf package and disabling systemd-resolved.

After that, create a file /etc/NetworkManager/dnsmasq.d/dev.europa.eu
```
address=/dev.europa.eu/10.0.2.15
```

To test if it works, the following command should return 10.0.2.15
```
nslookup dev.europa.eu
```


Proxy:

Create a file at ~/.docker/config.json

Add the following:
```
{
 "proxies":
 {
   "default":
   {
     "httpProxy": "http://<user>:<password>@<proxy ip address>:<proxy port>",
     "httpsProxy": "http://<user>:<password>@<proxy ip address>:<proxy port>",
     "noProxy": "localhost, 127.0.0.1, 172.16.0.0/10, .dev.europa.eu"
   }
 }
}
```
This passes the proxy vars to the containers, allowing you to do build steps.


Create a drop-in at /etc/systemd/system/docker.service.d/http-proxy.conf:

Add the following:
```
[Service]
Environment="HTTP_PROXY=<user>:<password>@<proxy ip address>:<proxy port>"
Environment="HTTPS_PROXY=<user>:<password>@<proxy ip address>:<proxy port>"
Environment="NO_PROXY=localhost,127.0.0.1,.dev.europa.eu"
```
This allows you to pull images from docker repos.


Installation of a website:
Configure the variables in the .env file.
```
. docker-build.sh
```

To access the shell:
```
docker exec -u 1000 -ti <PROJECT_NAME>_php bash
```

Behat tests:
L
./toolkit/phing test-run-behat
