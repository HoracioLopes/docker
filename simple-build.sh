# Export the vars in .env into your shell:
export $(egrep -v '^#' .env | xargs)

CONFIG="-f docker-compose.yml -f docker-compose.dev.yml"

if [ $1 == "ci" ]
then
  CONFIG="-f docker-compose.yml -f docker-compose.ci.yml"
fi

cd ./code/${PROJECT_NAME} \
&& if [ -f build.develop.props ]; then rm build.develop.props ; fi \
&& touch build.develop.props \
&& echo "platform.package.version = ${PLATFORM_PACKAGE_VERSION}" >> build.develop.props \
&& echo "project.docroot = /var/www/html/${PROJECT_NAME}" >> build.develop.props \
&& echo "project.url.base = http://${PROJECT_BASE_URL}" >> build.develop.props \
&& echo "db.dl.filename = ${DB_DL_FILENAME}" >> build.develop.props \
&& echo "db.dl.username = ${DB_DL_USERNAME}" >> build.develop.props \
&& echo "db.dl.password = ${DB_DL_PASSWORD}" >> build.develop.props \
&& echo "package-name = ${DB_DL_FILENAME} " >> build.develop.props \
&& echo "db.name = ${DB_NAME}" >> build.develop.props \
&& echo "db.user = ${DB_USER}" >> build.develop.props \
&& echo "db.password = ${DB_PASSWORD}" >> build.develop.props \
&& echo "db.host = ${DB_HOST}" >> build.develop.props \
&& echo "db.port = ${DB_PORT}" >> build.develop.props \
&& echo "db.driver = ${DB_DRIVER}" >> build.develop.props \
&& echo "behat.base_url = http://${PROJECT_BASE_URL}" >> build.develop.props \
&& echo "behat.wd_host.url = ${BEHAT_WD_HOST_URL}" >> build.develop.props \
&& echo "behat.api.driver = ${BEHAT_API_DRIVER}" >> build.develop.props \
&& echo "solr.host = solr.${PROJECT_DOMAIN}" >> build.develop.props \
&& echo "solr.path = solr" >> build.develop.props \
&& echo "solr.type = drupal" >> build.develop.props \
&& echo "solr.url = \${solr.scheme}://\${solr.host}/\${solr.path}/\${solr.type}" >> build.develop.props \
&& cd ../../ \
&& sudo chown -R 1000:1000 code \
&& docker-compose $CONFIG up -d \
&& docker exec -u 1000 ${PROJECT_NAME}_php composer install \
&& docker exec -u 1000 ${PROJECT_NAME}_php ./toolkit/phing build-platform \
&& docker exec -u 1000 ${PROJECT_NAME}_php ./toolkit/phing build-subsite-dev \
&& docker exec -u 1000 ${PROJECT_NAME}_php ./toolkit/phing install-clone \
&& docker exec -u 1000 ${PROJECT_NAME}_php ./toolkit/drush -r build vset apachesolr_attachments_extract_using solr
