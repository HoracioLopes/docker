# Export the vars in .env into your shell:
export $(egrep -v '^#' .env | xargs)

# Get the public ip for the dns config.
export PUBLIC_IP=ifconfig enp0s3 | sed -nre '/^[^ ]+/{N;s/^([^ ]+).*inet *([^ ]+).*/\2/p}'

# Add the repo credentials to the url to clone.
if [[ -n "$GIT_USER" && -n "$GIT_PASS" ]]; then
  export PROJECT_REPO=$(sed "s|://|://${GIT_USER}:${GIT_PASS}@|g" <<< ${PROJECT_REPO})
fi

# Clone the repo.
if [ -d ./code/${PROJECT_NAME} ]; then
  echo "Folder exists, skipping."
else
  git clone ${PROJECT_REPO} ./code/${PROJECT_NAME}
fi \
&& cd ./code/${PROJECT_NAME} \
&& git checkout ${PROJECT_BRANCH} \
&& if [ -f build.develop.props ]; then rm build.develop.props ; fi \
&& touch build.develop.props \
&& echo "platform.package.version = ${PLATFORM_PACKAGE_VERSION}" >> build.develop.props \
&& echo "project.docroot = /var/www/html/${PROJECT_NAME}" >> build.develop.props \
&& echo "project.url.base = http://${PROJECT_BASE_URL}" >> build.develop.props \
&& echo "db.dl.filename = ${DB_DL_FILENAME}" >> build.develop.props \
&& echo "db.dl.username = ${DB_DL_USERNAME}" >> build.develop.props \
&& echo "db.dl.password = ${DB_DL_PASSWORD}" >> build.develop.props \
&& echo "package-name = ${DB_DL_FILENAME} " >> build.develop.props \
&& echo "db.name = ${DB_NAME}" >> build.develop.props \
&& echo "db.user = ${DB_USER}" >> build.develop.props \
&& echo "db.password = ${DB_PASSWORD}" >> build.develop.props \
&& echo "db.host = ${DB_HOST}" >> build.develop.props \
&& echo "db.port = ${DB_PORT}" >> build.develop.props \
&& echo "db.driver = ${DB_DRIVER}" >> build.develop.props \
&& echo "behat.base_url = http://${PROJECT_BASE_URL}" >> build.develop.props \
&& echo "behat.wd_host.url = ${BEHAT_WD_HOST_URL}" >> build.develop.props \
&& echo "behat.api.driver = ${BEHAT_API_DRIVER}" >> build.develop.props \
&& echo "solr.host = solr.dev.europa.eu" >> build.develop.props \
&& echo "solr.path = solr" >> build.develop.props \
&& echo "solr.type = drupal" >> build.develop.props \
&& echo "solr.url = \${solr.scheme}://\${solr.host}/\${solr.path}/\${solr.type}" >> build.develop.props \
&& docker-compose up -d \
&& docker exec -u 1000 -ti ${PROJECT_NAME}_php composer install \
&& docker exec -u 1000 -ti ${PROJECT_NAME}_php ./toolkit/phing build-platform \
&& docker exec -u 1000 -ti ${PROJECT_NAME}_php ./toolkit/phing build-subsite-dev \
&& docker exec -u 1000 -ti ${PROJECT_NAME}_php ./toolkit/phing install-clone \
&& docker exec -u 1000 -ti ${PROJECT_NAME}_php ./toolkit/drush -r build vset apachesolr_attachments_extract_using solr \
&& xdg-open $(docker exec -u 1000 -ti futurium-dev_php ./toolkit/drush -r build uli)
